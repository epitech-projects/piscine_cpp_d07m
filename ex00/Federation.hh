//
// Federation.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 10:53:42 2014 Jean Gravier
// Last update Tue Jan 14 12:50:10 2014 Jean Gravier
//

#ifndef FEDERATION_HH_
# define FEDERATION_HH_

#include <string>
#include "Warpsystem.hh"

namespace		Federation
{
  namespace		Starfleet
  {

    class		Ship
    {
    public:
      Ship(int, int, std::string, short);
      ~Ship();
      void		setupCore(WarpSystem::Core *);
      void		checkCore();

    private:
      int		_length;
      int		_width;
      std::string	_name;
      short		_maxWrap;
      WarpSystem::Core	*_core;
    };
  }

  class		Ship
  {
  public:
    Ship(int, int, std::string);
    ~Ship();
    void		setupCore(WarpSystem::Core *);
    void		checkCore();

  private:
    int		_length;
    int		_width;
    std::string	_name;
    short	_maxWrap;
    WarpSystem::Core	*_core;
  };

}

#endif /* !FEDERATION_HH_ */
