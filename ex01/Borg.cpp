//
// Borg.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:58:32 2014 Jean Gravier
// Last update Tue Jan 14 13:54:31 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Borg.hh"

Borg::Ship::Ship()
{
  this->_side = 300;
  this->_maxWrap = 9;
  std::cout << "We are the Borgs. Lower your shields and surrender yourselves unconditionally." << std::endl;
  std::cout << "Your biological characteristics and technologies will be assimilated." << std::endl;
  std::cout << "Resistance is futile." << std::endl;
}

Borg::Ship::~Ship()
{

}

void		Borg::Ship::setupCore(WarpSystem::Core *core)
{
  this->_core = core;
}

void				Borg::Ship::checkCore()
{
  WarpSystem::QuantumReactor	*reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    std::cout << "Everything is in order." << std::endl;
  else
    std::cout << "Critical failure imminent." << std::endl;
}
