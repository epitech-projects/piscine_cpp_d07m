//
// Borg.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:55:05 2014 Jean Gravier
// Last update Tue Jan 14 13:10:07 2014 Jean Gravier
//

#ifndef BORG_HH_
#define BORG_HH_

#include "Warpsystem.hh"

namespace		Borg
{
  class			Ship
  {
  public:
    Ship();
    ~Ship();
    void		setupCore(WarpSystem::Core *);
    void		checkCore();
  private:
    int			_side;
    short		_maxWrap;
    WarpSystem::Core	*_core;
  };
}

#endif /* !BORG_HH_ */
