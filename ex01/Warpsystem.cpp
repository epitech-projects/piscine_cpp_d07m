//
// Warpsystem.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 11:18:36 2014 Jean Gravier
// Last update Tue Jan 14 12:44:36 2014 Jean Gravier
//

#include "Warpsystem.hh"
#include "Federation.hh"
#include <string>
#include <iostream>

WarpSystem::QuantumReactor::QuantumReactor()
{
  this->_stability = true;
}

WarpSystem::QuantumReactor::~QuantumReactor()
{

}

bool	WarpSystem::QuantumReactor::isStable()
{
  return (this->_stability);
}

void	WarpSystem::QuantumReactor::setStability(bool stability)
{
  this->_stability = stability;
}

WarpSystem::Core::Core(WarpSystem::QuantumReactor *coreReactor)
{
  this->_coreReactor = coreReactor;
}

WarpSystem::Core::~Core()
{

}

WarpSystem::QuantumReactor	*WarpSystem::Core::checkReactor()
{
  return (this->_coreReactor);
}
