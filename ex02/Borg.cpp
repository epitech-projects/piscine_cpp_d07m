//
// Borg.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:58:32 2014 Jean Gravier
// Last update Tue Jan 14 15:41:55 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Borg.hh"
#include "Destination.hh"

Borg::Ship::Ship()
{
  this->_side = 300;
  this->_maxWrap = 9;
  this->_home = UNICOMPLEX;
  this->_location = this->_home;

  std::cout << "We are the Borgs. Lower your shields and surrender yourselves unconditionally." << std::endl;
  std::cout << "Your biological characteristics and technologies will be assimilated." << std::endl;
  std::cout << "Resistance is futile." << std::endl;
}

Borg::Ship::~Ship()
{

}

void		Borg::Ship::setupCore(WarpSystem::Core *core)
{
  this->_core = core;
}

void				Borg::Ship::checkCore()
{
  WarpSystem::QuantumReactor	*reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    std::cout << "Everything is in order." << std::endl;
  else
    std::cout << "Critical failure imminent." << std::endl;
}

bool		Borg::Ship::move(int warp, Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move(int warp)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move(Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move()
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);

}
