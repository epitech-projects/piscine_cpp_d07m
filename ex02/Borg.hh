//
// Borg.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:55:05 2014 Jean Gravier
// Last update Tue Jan 14 15:42:21 2014 Jean Gravier
//

#ifndef BORG_HH_
#define BORG_HH_

#include "Warpsystem.hh"
#include "Destination.hh"

namespace		Borg
{
  class			Ship
  {
  public:
    Ship();
    ~Ship();
    void		setupCore(WarpSystem::Core *);
    void		checkCore();
      bool		move(int, Destination);
      bool		move(int);
      bool		move(Destination);
      bool		move();

  private:
    int			_side;
    short		_maxWrap;
    WarpSystem::Core	*_core;
    Destination		_location;
    Destination		_home;
};
}

#endif /* !BORG_HH_ */
