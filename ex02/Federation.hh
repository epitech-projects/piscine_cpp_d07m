//
// Federation.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 10:53:42 2014 Jean Gravier
// Last update Tue Jan 14 16:58:29 2014 Jean Gravier
//

#ifndef FEDERATION_HH_
# define FEDERATION_HH_

#include <string>
#include "Warpsystem.hh"
#include "Destination.hh"

namespace		Federation
{
  namespace		Starfleet
  {

    class		Captain
    {
    public:
      Captain(std::string);
      ~Captain();
      std::string	getName();
      int		getAge();
      void		setAge(int);
    private:
      std::string	_name;
      int		_age;
    };

    class		Ship
    {
    public:
      Ship(int, int, std::string, short);
      ~Ship();
      void		setupCore(WarpSystem::Core *);
      void		checkCore();
      void		promote(Federation::Starfleet::Captain *);
      bool		move(int, Destination);
      bool		move(int);
      bool		move(Destination);
      bool		move();

    private:
      int		_length;
      int		_width;
      std::string	_name;
      short		_maxWrap;
      WarpSystem::Core	*_core;
      Federation::Starfleet::Captain *_captain;
      Destination	_location;
      Destination	_home;
    };

    class		Ensign
    {
    public:
      Ensign(std::string);
      ~Ensign();

    private:
      std::string	_name;
    };

  }

  class		Ship
  {
  public:
    Ship(int, int, std::string);
    ~Ship();
    void		setupCore(WarpSystem::Core *);
    void		checkCore();
      bool		move(int, Destination);
      bool		move(int);
      bool		move(Destination);
      bool		move();

  private:
    int		_length;
    int		_width;
    std::string	_name;
    short	_maxWrap;
    WarpSystem::Core	*_core;
    Destination	_location;
    Destination	_home;
  };

}

#endif /* !FEDERATION_HH_ */
