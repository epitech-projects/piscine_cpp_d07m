//
// Borg.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:58:32 2014 Jean Gravier
// Last update Tue Jan 14 18:32:21 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Borg.hh"

Borg::Ship::Ship(int wF, short repair)
{
  this->_side = 300;
  this->_maxWrap = 9;
  this->_home = UNICOMPLEX;
  this->_location = this->_home;
  this->_shield = 100;
  this->_weaponFrequency = wF;
  this->_repair = repair;

  std::cout << "We are the Borgs. Lower your shields and surrender yourselves unconditionally." << std::endl;
  std::cout << "Your biological characteristics and technologies will be assimilated." << std::endl;
  std::cout << "Resistance is futile." << std::endl;
}

Borg::Ship::Ship(int wF)
{
  this->_side = 300;
  this->_maxWrap = 9;
  this->_home = UNICOMPLEX;
  this->_location = this->_home;
  this->_shield = 100;
  this->_weaponFrequency = wF;
  this->_repair = 3;

  std::cout << "We are the Borgs. Lower your shields and surrender yourselves unconditionally." << std::endl;
  std::cout << "Your biological characteristics and technologies will be assimilated." << std::endl;
  std::cout << "Resistance is futile." << std::endl;
}

Borg::Ship::~Ship()
{

}

void		Borg::Ship::setupCore(WarpSystem::Core *core)
{
  this->_core = core;
}

void				Borg::Ship::checkCore()
{
  WarpSystem::QuantumReactor	*reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    std::cout << "Everything is in order." << std::endl;
  else
    std::cout << "Critical failure imminent." << std::endl;
}

bool		Borg::Ship::move(int warp, Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move(int warp)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move(Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Borg::Ship::move()
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);

}

int		Borg::Ship::getShield()
{
  return (this->_shield);
}
void		Borg::Ship::setShield(int shield)
{
  this->_shield = shield;
}
int		Borg::Ship::getWeaponFrequency()
{
  return (this->_weaponFrequency);
}
void		Borg::Ship::setWeaponFrequency(int wF)
{
  this->_weaponFrequency = wF;
}
short		Borg::Ship::getRepair()
{
  return (this->_repair);
}
void		Borg::Ship::setRepair(short repair)
{
  this->_repair = repair;
}

void		Borg::Ship::fire(Federation::Starfleet::Ship *ship)
{
  ship->setShield(ship->getShield() - this->_weaponFrequency);
  std::cout << "Firing on target with " << this->_weaponFrequency << "GW frequency." << std::endl;
}

void				Borg::Ship::fire(Federation::Ship *ship)
{
  WarpSystem::Core		*core;
  WarpSystem::QuantumReactor	*reactor;

  core = ship->getCore();
  reactor = core->checkReactor();
  reactor->setStability(false);
  std::cout << "Firing on target with " << this->_weaponFrequency << "GW frequency." << std::endl;
}

void		Borg::Ship::repair()
{
  if (this->_repair > 0)
    {
      this->_repair--;
      this->_shield = 100;
      std::cout << "Begin shield re-initialisation... Done. Awaiting further instructions." << std::endl;
    }
  else
    std::cout << "Energy cells depleted, shield weakening." << std::endl;
}
