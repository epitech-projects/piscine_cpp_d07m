//
// Borg.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 12:55:05 2014 Jean Gravier
// Last update Tue Jan 14 18:32:33 2014 Jean Gravier
//

#ifndef BORG_HH_
# define BORG_HH_

#include "Warpsystem.hh"
#include "Destination.hh"
#include "Federation.hh"

namespace Federation
{
  namespace Starfleet
  {
    class Ship;
  }
}

namespace		Borg
{
  class			Ship
  {
  public:
    Ship(int, short);
    Ship(int);
    ~Ship();
    void		setupCore(WarpSystem::Core *);
    void		checkCore();
    bool		move(int, Destination);
    bool		move(int);
    bool		move(Destination);
    bool		move();
    int			getShield();
    void		setShield(int);
    int			getWeaponFrequency();
    void		setWeaponFrequency(int);
    short		getRepair();
    void		setRepair(short);
    void		fire(Federation::Starfleet::Ship *);
    void		fire(Federation::Ship *);
    void		repair();

  private:
    int			_side;
    short		_maxWrap;
    WarpSystem::Core	*_core;
    Destination		_location;
    Destination		_home;
    int			_shield;
    int			_weaponFrequency;
    short		_repair;
  };
}

#endif /* !BORG_HH_ */
