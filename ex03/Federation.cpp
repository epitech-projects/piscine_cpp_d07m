//
// Federation.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 11:04:43 2014 Jean Gravier
// Last update Tue Jan 14 20:50:36 2014 Jean Gravier
//

#include "Federation.hh"
#include "Borg.hh"
#include <string>
#include <iostream>

Federation::Starfleet::Ship::Ship(int length, int width, std::string name, short maxWrap, int torpedo)
{
  this->_length = length;
  this->_width = width;
  this->_name = name;
  this->_maxWrap = maxWrap;
  this->_home = EARTH;
  this->_location = this->_home;
  this->_shield = 100;
  this->_photonTorpedo = torpedo;

  std::cout << "The ship USS " << this->_name << " has been finished. It is " << this->_length
	    << " m in length and " << this->_width << " m in width. It can go to Warp "
	    << this->_maxWrap << "! Weapons are set: " << this->_photonTorpedo
	    << " torpedoes ready." << std::endl;
}

Federation::Starfleet::Ship::Ship(void)
{
  this->_length = 289;
  this->_width = 132;
  this->_name = "Entreprise";
  this->_maxWrap = 6;
  this->_home = EARTH;
  this->_location = this->_home;
  this->_shield = 100;
  this->_photonTorpedo = 20;

  std::cout << "The ship USS " << this->_name << " has been finished. It is " << this->_length
	    << " m in length and " << this->_width << " m in width. It can go to Warp "
	    << this->_maxWrap << "! Weapons are set: " << this->_photonTorpedo
	    << " torpedoes ready." << std::endl;
}

Federation::Starfleet::Ship::~Ship()
{

}

Federation::Ship::Ship(int length, int width, std::string name)
{
  this->_length = length;
  this->_width = width;
  this->_name = name;
  this->_maxWrap = 1;
  this->_home = VULCAN;
  this->_location = this->_home;

  std::cout << "The independant ship " << this->_name << " just finished its construction. It is " << this->_length
	    << " m in length and " << this->_width << " m in width." << std::endl;
}

Federation::Ship::~Ship()
{

}

void		Federation::Starfleet::Ship::setupCore(WarpSystem::Core *core)
{
  this->_core = core;
  std::cout << "USS " << this->_name << ": The core is set." << std::endl;
}

void				Federation::Starfleet::Ship::checkCore()
{
  WarpSystem::QuantumReactor	*reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    std::cout << "USS " << this->_name << ": The core is stable at the time." << std::endl;
  else
    std::cout << "USS " << this->_name << ": The core is unstable at the time." << std::endl;
}

void		Federation::Starfleet::Ship::promote(Federation::Starfleet::Captain *captain)
{
  this->_captain = captain;
  std::cout << captain->getName() << ": I'm glad to be the captain of the USS " << this->_name << "." << std::endl;
}

bool		Federation::Starfleet::Ship::move(int warp, Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Federation::Starfleet::Ship::move(int warp)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);
}

bool		Federation::Starfleet::Ship::move(Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Federation::Starfleet::Ship::move()
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);

}

int		Federation::Starfleet::Ship::getShield()
{
  return (this->_shield);
}

void		Federation::Starfleet::Ship::setShield(int shield)
{
  this->_shield = shield;
}

int		Federation::Starfleet::Ship::getTorpedo()
{
  return (this->_photonTorpedo);
}

void		Federation::Starfleet::Ship::setTorpedo(int torpedo)
{
  this->_photonTorpedo = torpedo;
}

void		Federation::Starfleet::Ship::fire(Borg::Ship *ship)
{
  if (this->getTorpedo())
    {
      if ((this->_photonTorpedo - 1) >= 0)
	{
	  this->_photonTorpedo--;
	  std::cout << this->_name << ": Firing on target. " << this->_photonTorpedo << " torpedoes remaining." << std::endl;
	  ship->setShield(ship->getShield() - 50);
	}
      else
	std::cout << this->_name << ": No enough torpedoes to fire, " << this->_captain->getName() << "!" << std::endl;
    }
  else
    std::cout << this->_name << ": No more torpedo to fire, " << this->_captain->getName() << "!" << std::endl;
}

void		Federation::Starfleet::Ship::fire(int torpedoes, Borg::Ship *ship)
{
  if (this->getTorpedo())
    {
      if ((this->_photonTorpedo - torpedoes) >= 0)
	{
	  this->_photonTorpedo -= torpedoes;
	  std::cout << this->_name << ": Firing on target. " << this->_photonTorpedo << " torpedoes remaining." << std::endl;
	  ship->setShield(ship->getShield() - (50 * torpedoes));
	}
      else
	std::cout << this->_name << ": No enough torpedoes to fire, " << this->_captain->getName() << "!" << std::endl;
    }
  else
    std::cout << this->_name << ": No more torpedo to fire, " << this->_captain->getName() << "!" << std::endl;
}

void		Federation::Ship::setupCore(WarpSystem::Core *core)
{
  this->_core = core;
  std::cout << this->_name << ": The core is set." << std::endl;
}

void				Federation::Ship::checkCore()
{
  WarpSystem::QuantumReactor	*reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    std::cout << this->_name << ": The core is stable at the time." << std::endl;
  else
    std::cout << this->_name << ": The core is unstable at the time." << std::endl;
}

bool		Federation::Ship::move(int warp, Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Federation::Ship::move(int warp)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (warp <= this->_maxWrap && reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);
}

bool		Federation::Ship::move(Destination d)
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (d != this->_location && reactor->isStable())
    {
      this->_location = d;
      return (true);
    }
  return (false);
}

bool		Federation::Ship::move()
{
  WarpSystem::QuantumReactor *reactor;

  reactor = this->_core->checkReactor();
  if (reactor->isStable())
    {
      this->_location = this->_home;
      return (true);
    }
  return (false);
}

WarpSystem::Core	*Federation::Ship::getCore()
{
  return (this->_core);
}

Federation::Starfleet::Captain::Captain(std::string name)
{
  this->_name = name;
}

Federation::Starfleet::Captain::~Captain()
{

}

std::string	Federation::Starfleet::Captain::getName()
{
  return (this->_name);
}

int	Federation::Starfleet::Captain::getAge()
{
  return (this->_age);
}

void	Federation::Starfleet::Captain::setAge(int age)
{
  this->_age = age;
}

Federation::Starfleet::Ensign::Ensign(std::string name)
{
  this->_name = name;
  std::cout << "Ensign " << this->_name << ", awaiting orders." << std::endl;
}

Federation::Starfleet::Ensign::~Ensign()
{

}
