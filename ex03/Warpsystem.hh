//
// Warpsystem.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d07m/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 11:18:40 2014 Jean Gravier
// Last update Tue Jan 14 17:52:04 2014 Jean Gravier
//

#ifndef WARPSYSTEM_HH_
# define WARPSYSTEM_HH_

namespace	WarpSystem
{
  class		QuantumReactor
  {
  public:
    QuantumReactor();
    ~QuantumReactor();
    bool	isStable();
    void	setStability(bool);

  private:
    bool	_stability;
  };

  class			Core
  {
  public:
    Core(WarpSystem::QuantumReactor *);
    ~Core();
    WarpSystem::QuantumReactor	*checkReactor();

  private:
    WarpSystem::QuantumReactor	*_coreReactor;
  };
}

#endif /* !WARPSYSTEM_HH_ */
